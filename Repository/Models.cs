﻿using SQLite;

namespace HonkHonkBot.Repository;

public class Entry(long? userId, long chatId, DateTime lastTriggerTimestamp)
{
    [PrimaryKey, AutoIncrement] public long Id { get; set; }

    public long? UserId { get; set; } = userId;

    public long ChatId { get; set; } = chatId;

    public DateTime LastTriggerTimestamp { get; set; } = lastTriggerTimestamp;

    public Entry() : this(default, default, default)
    {
    }
}

public class ChatSettings(
    long chatId,
    bool quietMode,
    DateTime lastTriggerTimestamp,
    DateTime nextTriggerTimestamp,
    string language)
{
    [PrimaryKey, AutoIncrement] public long Id { get; set; }

    public long ChatId { get; set; } = chatId;

    public bool QuietMode { get; set; } = quietMode;

    public DateTime LastTriggerTimestamp { get; set; } = lastTriggerTimestamp;

    public DateTime NextTriggerTimestamp { get; set; } = nextTriggerTimestamp;

    public string Language { get; set; } = language;

    public ChatSettings() : this(default, default, DateTime.UtcNow, DateTime.UtcNow.GetRandomNextTimestamp(), "en")
    {
    }
}