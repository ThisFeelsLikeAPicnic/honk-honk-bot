﻿namespace HonkHonkBot.Repository;

public interface IRepository
{
    Task InitializeAsync(CancellationToken cancellationToken = default);

    Task<Entry?> GetEntryOrDefaultAsync(long? userId, long chatId, CancellationToken cancellationToken = default);

    Task<int> UpdateEntryAsync(Entry updatedEntry, CancellationToken cancellationToken = default);

    Task<int> CreateEntryAsync(long? userId, long chatId, CancellationToken cancellationToken = default);

    Task<int> RemoveEntryAsync(Entry entry, CancellationToken cancellationToken = default);

    Task<ChatSettings?> GetChatSettingsOrDefaultAsync(long chatId, CancellationToken cancellationToken = default);

    Task<int> UpdateChatSettingsAsync(ChatSettings updatedChatSettings,
        CancellationToken cancellationToken = default);

    Task<int> CreateChatSettingsAsync(long chatId, string language, CancellationToken cancellationToken = default);

    Task<IEnumerable<ChatSettings>> GetAllChatSettingsAsync(CancellationToken cancellationToken = default);
}