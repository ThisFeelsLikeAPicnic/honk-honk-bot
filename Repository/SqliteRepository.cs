﻿using SQLite;

namespace HonkHonkBot.Repository;

public class SqliteRepository(SQLiteAsyncConnection sqliteConnection) : IRepository
{
    public Task InitializeAsync(CancellationToken cancellationToken = default) =>
        Task.WhenAll(sqliteConnection.EnableWriteAheadLoggingAsync(),
            sqliteConnection.CreateTablesAsync<Entry, ChatSettings>());

    public Task<Entry?> GetEntryOrDefaultAsync(long? userId, long chatId,
        CancellationToken cancellationToken = default) =>
        sqliteConnection.FindAsync<Entry?>(e => e!.UserId == userId && e.ChatId == chatId);

    public Task<int> UpdateEntryAsync(Entry updatedEntry, CancellationToken cancellationToken = default) =>
        sqliteConnection.UpdateAsync(updatedEntry);

    public Task<int> CreateEntryAsync(long? userId, long chatId,
        CancellationToken cancellationToken = default) =>
        sqliteConnection.InsertAsync(new Entry(userId, chatId, DateTime.UtcNow));

    public Task<int> RemoveEntryAsync(Entry entry, CancellationToken cancellationToken = default) =>
        sqliteConnection.DeleteAsync(entry);

    public Task<ChatSettings?> GetChatSettingsOrDefaultAsync(long chatId,
        CancellationToken cancellationToken = default) =>
        sqliteConnection.FindAsync<ChatSettings?>(s => s!.ChatId == chatId);

    public Task<int> UpdateChatSettingsAsync(ChatSettings updatedChatSettings,
        CancellationToken cancellationToken = default) => sqliteConnection.UpdateAsync(updatedChatSettings);

    public Task<int> CreateChatSettingsAsync(long chatId, string language,
        CancellationToken cancellationToken = default) =>
        sqliteConnection.InsertAsync(new ChatSettings(chatId, default, DateTime.UtcNow,
            DateTime.UtcNow.GetRandomNextTimestamp(), language));

    public async Task<IEnumerable<ChatSettings>> GetAllChatSettingsAsync(CancellationToken cancellationToken = default)
        => await sqliteConnection.Table<ChatSettings>().ToArrayAsync();
}