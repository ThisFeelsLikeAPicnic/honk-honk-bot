﻿namespace HonkHonkBot.Bot;

public interface ISpellCheckerService
{
    Task<bool> IsTextSpelledCorrectlyAsync(string? text, CancellationToken cancellationToken = default);
}