﻿using HonkHonkBot.Bot.Jokes;
using HonkHonkBot.Repository;
using Telegram.BotAPI;
using Telegram.BotAPI.AvailableMethods;

namespace HonkHonkBot.Bot;

public class JokesBackgroundService(
    ITelegramBotClient client,
    IRepository repository,
    IServiceProvider serviceProvider,
    ILogger<JokesBackgroundService> logger) : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        // we don't need to start right away, let's wait a bit
        await Task.Delay(TimeSpan.FromSeconds(10), stoppingToken);
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                // we don't actually need the fastest execution here, so we'll slowly work our way there
                var allChatSettings = await repository.GetAllChatSettingsAsync(stoppingToken);

                // Telegram Bot API enables rate-limiting when sending more than 30 messages in 1 second
                // so we'll give it some headroom by splitting chats array into smaller chunks
                await allChatSettings.ProcessArrayInChunksWithDelayAsync(15, TimeSpan.FromSeconds(1), async s =>
                {
                    if (s.QuietMode)
                        return;

                    var triggerTimestamp = DateTime.UtcNow;
                    if (triggerTimestamp < s.NextTriggerTimestamp)
                        return;

                    var joke = await serviceProvider.GetRequiredKeyedService<IJokeService>(LanguageCodes.Russian)
                        .GetRandomJokeAsync(stoppingToken);

                    if (joke.IsNullOrEmptyOrWhiteSpace())
                        return;

                    await client.SendMessageAsync(s.ChatId, joke!, cancellationToken: stoppingToken);

                    s.NextTriggerTimestamp = triggerTimestamp.GetRandomNextTimestamp();
                    s.LastTriggerTimestamp = triggerTimestamp;

                    await repository.UpdateChatSettingsAsync(s, stoppingToken);

                    logger.LogInformation("Sent a new random joke - chat_id: {ChatId}", s.ChatId);
                }, stoppingToken);
            }
            catch (TaskCanceledException)
            {
                // ignored
            }
            catch (Exception e)
            {
                logger.LogError(e, "{Message}", e.Message);
            }
            finally
            {
                await Task.Delay(TimeSpan.FromMinutes(1), stoppingToken);
            }
        }
    }
}