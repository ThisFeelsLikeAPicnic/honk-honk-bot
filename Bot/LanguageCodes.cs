﻿namespace HonkHonkBot.Bot;

public enum LanguageCodes
{
    Russian,
    English
}