﻿using HonkHonkBot.Repository;

namespace HonkHonkBot.Bot;

public class BotBackgroundService(ICustomTelegramBot bot, IRepository repository) : BackgroundService
{
    public override Task StartAsync(CancellationToken cancellationToken) =>
        Task.WhenAll(bot.InitializeAsync(cancellationToken), repository.InitializeAsync(cancellationToken))
            .ContinueWith(_ => base.StartAsync(cancellationToken), cancellationToken)
            .Unwrap();

    protected override Task ExecuteAsync(CancellationToken stoppingToken) =>
        bot.ExecuteLongPollAsync(stoppingToken);
}