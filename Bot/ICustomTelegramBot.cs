﻿namespace HonkHonkBot.Bot;

public interface ICustomTelegramBot
{
    Task InitializeAsync(CancellationToken cancellationToken = default);

    Task ExecuteLongPollAsync(CancellationToken cancellationToken = default);
}