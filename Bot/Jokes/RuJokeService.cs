﻿using System.Collections.Immutable;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace HonkHonkBot.Bot.Jokes;

public class RuJokeService(HttpClient client, ILogger<RuJokeService> logger) : IJokeService
{
    private readonly Regex _contentCleaner = new("(^{\"content\":\"|\"}$)", RegexOptions.Compiled);

    private readonly ImmutableArray<int> _apiCategories =
        ImmutableArray.Create(1, 2, 3, 4, 5, 6, 8, 11, 12, 13, 14, 15, 16, 18);

    public async Task<string?> GetRandomJokeAsync(CancellationToken cancellationToken = default)
    {
        var result = string.Empty;
        try
        {
            var response = await client.GetStringAsync(
                $"http://rzhunemogu.ru/RandJSON.aspx?CType={_apiCategories[RandomNumberGenerator.GetInt32(_apiCategories.Length)]}",
                cancellationToken);

            if (response.IsNullOrEmptyOrWhiteSpace())
                return default;

            result = _contentCleaner.Replace(response, string.Empty);
        }
        catch (Exception e)
        {
            logger.LogError(e, "{Message}", e.Message);
        }

        return result.IsNullOrEmptyOrWhiteSpace()
            ? default
            : result;
    }
}