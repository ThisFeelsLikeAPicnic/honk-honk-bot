﻿using HtmlAgilityPack;

namespace HonkHonkBot.Bot.Jokes;

public class RuV2JokeService(HttpClient client, ILogger<RuJokeService> logger) : IJokeService
{
    public async Task<string?> GetRandomJokeAsync(CancellationToken cancellationToken = default)
    {
        var result = default(Stream);
        try
        {
            result = await client.GetStreamAsync("https://baneks.ru/random", cancellationToken);
        }
        catch (Exception e)
        {
            logger.LogError(e, "{Message}", e.Message);
        }

        if (result is null)
            return default;

        var document = new HtmlDocument();
        document.Load(result);

        if (document.DocumentNode?.InnerHtml.IsNullOrEmptyOrWhiteSpace() is not false)
            return default;

        var textNode = document.DocumentNode.SelectSingleNode("//section[@class='anek-view']/article/p");
        return textNode?.InnerText;
    }
}