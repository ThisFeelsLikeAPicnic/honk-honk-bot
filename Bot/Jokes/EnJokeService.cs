﻿namespace HonkHonkBot.Bot.Jokes;

public class EnJokeService(HttpClient client, ILogger<EnJokeService> logger) : IJokeService
{
    public async Task<string?> GetRandomJokeAsync(CancellationToken cancellationToken = default)
    {
        var result = string.Empty;
        try
        {
            client.DefaultRequestHeaders.Add("Accept", "text/plain");
            result = await client.GetStringAsync("https://icanhazdadjoke.com/", cancellationToken);
        }
        catch (Exception e)
        {
            logger.LogError(e, "{Message}", e.Message);
        }

        return result.IsNullOrEmptyOrWhiteSpace()
            ? default
            : result;
    }
}