﻿namespace HonkHonkBot.Bot.Jokes;

public interface IJokeService
{
    Task<string?> GetRandomJokeAsync(CancellationToken cancellationToken = default);
}