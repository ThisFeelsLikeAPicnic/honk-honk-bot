﻿using System.Net.Http.Json;
using System.Text.Json;
using System.Web;

namespace HonkHonkBot.Bot;

public class SpellCheckerService(HttpClient client, ILogger<SpellCheckerService> logger) : ISpellCheckerService
{
    private readonly JsonSerializerOptions _jsonSerializerOptions = new() { IncludeFields = true };

    public async Task<bool> IsTextSpelledCorrectlyAsync(string? text, CancellationToken cancellationToken = default)
    {
        if (text.IsNullOrEmptyOrWhiteSpace())
            return true;

        var response = Array.Empty<YandexSpellerApiResponse>();
        try
        {
            response = await client.GetFromJsonAsync<YandexSpellerApiResponse[]>(
                $"https://speller.yandex.net/services/spellservice.json/checkText?options=14&text={HttpUtility.UrlEncode(text)}",
                _jsonSerializerOptions,
                cancellationToken);
        }
        catch (Exception e)
        {
            logger.LogError(e, "{Message}", e.Message);
        }

        if (response is null || response.Length == 0)
            return true;

        return !response.Any(r =>
            r.code is YandexSpellerApiErrorCodes.UnknownWord or YandexSpellerApiErrorCodes.Capitalization
            && !r.s.Contains(r.word));
    }
}

#region Type declarations

internal enum YandexSpellerApiErrorCodes
{
    UnknownWord = 1,
    RepeatWord = 2,
    Capitalization = 3,
    TooManyErrors = 4
}

internal class YandexSpellerApiResponse
{
    public YandexSpellerApiErrorCodes code;
    public int pos;
    public int row;
    public int col;
    public int len;
    public string word;
    public string[] s;
}

#endregion