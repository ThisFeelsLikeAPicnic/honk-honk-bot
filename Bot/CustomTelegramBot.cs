using System.Collections.Immutable;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Catalyst;
using HonkHonkBot.Bot.Commands;
using HonkHonkBot.Repository;
using Microsoft.Extensions.Options;
using Mosaik.Core;
using Telegram.BotAPI;
using Telegram.BotAPI.AvailableMethods;
using Telegram.BotAPI.AvailableTypes;
using Telegram.BotAPI.Extensions;
using Telegram.BotAPI.GettingUpdates;
using Document = Catalyst.Document;

namespace HonkHonkBot.Bot;

public class CustomTelegramBot(
    ITelegramBotClient client,
    IOptions<BotSettings> settings,
    IRepository repository,
    ISpellCheckerService spellCheckerService,
    Pipeline pipeline,
    IEnumerable<CustomBotCommand> commands,
    ILogger<CustomTelegramBot> logger) : SimpleTelegramBotBase, ICustomTelegramBot
{
    private const string BotName = "HONK-HONK";
    private const string BotShortDescription = "I SPREAD THE HONK-HONK CURSE FAR AND WIDE!";
    private const string BotDescription = "I SPREAD THE HONK-HONK CURSE FAR AND WIDE!";
    private const string GreetingMessage = "Sup, bitches";

    private readonly Regex _imperativeVerbRegex = new(
        "(?:(?:[^х]у|[абвгдеёжзкмнопрстфхцчшщюэя])[ий]|[бвгджзйклмнрсфч][ь])(?<remove>-ка|(?<!ди)с[ья]|те)*?$",
        RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

    private readonly ImmutableArray<string> _imperativeOptions = ImmutableArray.Create("Анус себе {0}, пёс",
        "Ебыч себе {0} лучше", "По губам себе {0}, уёба");

    private readonly ImmutableArray<string> _imperativeSpecialOptions = ImmutableArray.Create("{0} на хуй, сука",
        "{0} в жопу, пидор", "{0} в пизду, шлюха");

    private readonly ImmutableArray<string> _imperativeSpecials = ImmutableArray.Create("беги", "ехай", "езжай", "иди",
        "сходи", "катись", "лезь", "несись", "носись", "плыви", "ползи", "пойди");

    private readonly ImmutableArray<string> _availableUpdates = ImmutableArray.Create("message", "my_chat_member",
        "edited_message");

    private readonly ImmutableArray<char> _cyrillicConsonants = ImmutableArray.Create('б', 'в', 'г', 'д', 'ж', 'з', 'к',
        'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ');

    private readonly Dictionary<char, char> _cyrillicMappings = new()
    {
        { 'а', 'я' }, { 'о', 'ё' }, { 'р', 'л' }, { 'у', 'ю' }, { 'ч', 'т' }, { 'ш', 'с' }, { 'щ', 'с' }, { 'ы', 'и' },
        { 'э', 'е' }, { 'е', 'и' }
    };

    private readonly Dictionary<char, char> _latinToCyrillicMappings = new()
    {
        { 'a', 'а' }, { 'b', 'б' }, { 'c', 'ц' }, { 'd', 'д' }, { 'e', 'е' }, { 'f', 'ф' }, { 'g', 'г' }, { 'h', 'х' },
        { 'i', 'и' }, { 'j', 'ж' }, { 'k', 'к' }, { 'l', 'л' }, { 'm', 'м' }, { 'n', 'н' }, { 'o', 'о' }, { 'p', 'п' },
        { 'q', 'к' }, { 'r', 'р' }, { 's', 'с' }, { 't', 'т' }, { 'u', 'у' }, { 'v', 'в' }, { 'w', 'в' }, { 'x', 'х' },
        { 'y', 'й' }, { 'z', 'з' }
    };

    private long? _godChatId;
    private User _botInfo = null!;

    public async Task InitializeAsync(CancellationToken cancellationToken = default)
    {
        _botInfo = await client.GetMeAsync(cancellationToken);
        SetCommandExtractor(_botInfo.Username!);

        var initializationTasks = new[]
        {
            // we don't need to update bot info too often, otherwise we'll catch rate-limit on these methods
            BotName.IsNullOrEmptyOrWhiteSpace() || _botInfo.FirstName == BotName
                ? Task.CompletedTask
                : client.SetMyNameAsync(BotName, cancellationToken: cancellationToken),
            BotShortDescription.IsNullOrEmptyOrWhiteSpace()
                ? Task.CompletedTask
                : client.SetMyShortDescriptionAsync(BotShortDescription, cancellationToken: cancellationToken),
            BotDescription.IsNullOrEmptyOrWhiteSpace()
                ? Task.CompletedTask
                : client.SetMyDescriptionAsync(BotDescription, cancellationToken: cancellationToken),
            !commands.Any()
                ? Task.CompletedTask
                : client.SetMyCommandsAsync(commands, cancellationToken: cancellationToken)
        };

        try
        {
            await Task.WhenAll(initializationTasks);
        }
        catch (BotRequestException e)
        {
            logger.LogError("Caught error during initialization - {Message}", e.Message);

            // we'll skip 429 Rate-Limit
            // any other exception must stop application
            if (e.ErrorCode != 429)
                throw;
        }

        if (long.TryParse(Environment.GetEnvironmentVariable("GOD_CHAT_ID"), out var godChatId))
            _godChatId = godChatId;

        logger.LogInformation("Started bot - id:{Id}, username:{Username}", _botInfo.Id, _botInfo.Username);
    }

    public async Task ExecuteLongPollAsync(CancellationToken cancellationToken = default)
    {
        int? offset = null;
        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                var updateTasks = (await client.GetUpdatesAsync(offset, allowedUpdates: _availableUpdates,
                        cancellationToken: cancellationToken))
                    .Select(u => OnUpdateAsync(u, cancellationToken).ContinueWith(_ => u.UpdateId, cancellationToken));

                var processedUpdateIds = await Task.WhenAll(updateTasks);
                if (processedUpdateIds.Length == 0)
                    continue;

                offset = processedUpdateIds.Max();
                offset++;
            }
            catch (TaskCanceledException)
            {
                // ignored
            }
            catch (Exception e)
            {
                logger.LogError(e, "{Message}", e.Message);
            }
        }
    }

    protected override Task OnMyChatMemberAsync(ChatMemberUpdated myChatMember,
        CancellationToken cancellationToken = default)
    {
        if (myChatMember.NewChatMember is ChatMemberMember newChatMember && newChatMember.User.Id == _botInfo.Id)
        {
            return Task.WhenAll(
                client.SendMessageAsync(myChatMember.Chat.Id, GreetingMessage, cancellationToken: cancellationToken),
                repository.CreateChatSettingsAsync(myChatMember.Chat.Id, "ru", cancellationToken)
            );
        }

        return Task.CompletedTask;
    }

    protected override async Task OnMessageAsync(Message message, CancellationToken cancellationToken = default)
    {
        if (message is not { LeftChatMember: null, NewChatMembers: null })
            return;

        // skip service messages
        if (message.From?.Id == TelegramConstants.TelegramId)
            return;

        if (message.Entities?.Any(e => e.Type == "bot_command") is true)
        {
            // use default command parsing
            await base.OnMessageAsync(message, cancellationToken);
            return;
        }

        var alreadyAnswered = false;
        var chatSettings = await repository.GetChatSettingsOrDefaultAsync(message.Chat.Id, cancellationToken);
        if (_godChatId.HasValue
            && ((message.ReplyToMessage is { } reply
                 && reply.Chat.Id == message.Chat.Id
                 && reply.From?.Id == _botInfo.Id)
                || (message.Entities?.Any(e => e.Type == "mention") is true
                    && message.Text?.Contains($"@{_botInfo.Username}") is true)))
        {
            if (message.Chat.Id == _godChatId
                && message.ReplyToMessage?.ExternalReply is { Chat: not null, MessageId: not null } extReply)
            {
                var extChatSettings =
                    await repository.GetChatSettingsOrDefaultAsync(extReply.Chat.Id, cancellationToken);

                if (extChatSettings is not null)
                    await client.SendMessageAsync(extReply.Chat.Id, message.Text ?? string.Empty,
                        replyParameters: new ReplyParameters
                        {
                            AllowSendingWithoutReply = true,
                            ChatId = extReply.Chat.Id,
                            MessageId = extReply.MessageId.Value
                        }, cancellationToken: cancellationToken);
            }
            else if (message.Chat.Id != _godChatId && chatSettings is not null)
            {
                await client.SendMessageAsync(_godChatId.Value, message.Text ?? message.Caption ?? string.Empty,
                    replyParameters: message.CreateReplyParameters(), cancellationToken: cancellationToken);
            }
        }

        // TODO: do that for any language
        if (chatSettings?.QuietMode is not true && !alreadyAnswered)
        {
            var outputDoc = pipeline.ProcessSingle(
                new Document(message.Text ?? message.Caption ?? string.Empty, Language.Russian),
                cancellationToken);

            var verbs = outputDoc?.ToTokenList()
                .Where(t => t.POS is PartOfSpeech.VERB && !t.Value.IsNullOrEmptyOrWhiteSpace())
                .Select(t => t.Value)
                .ToArray() ?? Array.Empty<string>();

            if (verbs.Length > 0)
            {
                var verb = verbs[RandomNumberGenerator.GetInt32(verbs.Length)].ToLower();
                var imperativeVerb = _imperativeVerbRegex.Match(verb);
                if (imperativeVerb.Success && RandomNumberGenerator.GetInt32(100) % 9 == 0)
                {
                    if (imperativeVerb.Groups["remove"].Success)
                        verb = verb[..imperativeVerb.Groups["remove"].Index];

                    var replyText = verb switch
                    {
                        not null when _imperativeSpecials.Contains(verb) =>
                            string.Format(_imperativeSpecialOptions.GetRandomItem(), char.ToUpper(verb[0]) + verb[1..]),
                        _ => string.Format(_imperativeOptions.GetRandomItem(), verb)
                    };

                    await client.SendMessageAsync(message.Chat.Id, replyText,
                        replyParameters: message.CreateReplyParameters(),
                        cancellationToken: cancellationToken);

                    alreadyAnswered = true;
                }
            }
        }

        var entry = await repository.GetEntryOrDefaultAsync(message.From?.Id, message.Chat.Id, cancellationToken);
        if (entry is null)
            if (await spellCheckerService.IsTextSpelledCorrectlyAsync(message.Text ?? message.Caption,
                    cancellationToken))
                return;

        logger.LogInformation("Received message - message_id:{MessageId}, chat_id:{ChatId}",
            message.MessageId, message.Chat.Id);

        await client.SetMessageReactionAsync(message.Chat.Id, message.MessageId, Helpers.DefaultReactions, false,
            cancellationToken);

        if (entry is null || alreadyAnswered)
            return;

        var text = message.Text ?? message.Caption;
        if (chatSettings?.QuietMode is false
            && RandomNumberGenerator.GetInt32(60) == DateTime.UtcNow.Second
            && !text.IsNullOrEmptyOrWhiteSpace()
            && (text!.Contains("://") || text.Contains('@')))
        {
            var sb = new StringBuilder();
            for (var i = 0; i < text.Length; i++)
            {
                var current = text[i];
                var next = i + 1 >= text.Length
                    ? '\0'
                    : text[i + 1];

                if (char.IsLetter(current))
                {
                    var currentCaseInsensitive = char.ToLower(current);
                    if (_cyrillicMappings.TryGetValue(currentCaseInsensitive, out var replacement)
                        && RandomNumberGenerator.GetInt32(100) % 3 != 0)
                        current = char.IsUpper(current) ? char.ToUpper(replacement) : replacement;

                    if (_latinToCyrillicMappings.TryGetValue(currentCaseInsensitive, out replacement))
                        current = char.IsUpper(current) ? char.ToUpper(replacement) : replacement;

                    sb.Append(current);
                    if (_cyrillicConsonants.Contains(char.ToLower(current))
                        && (char.IsWhiteSpace(next) || char.IsPunctuation(next) || next == '\0'))
                        sb.Append(char.IsUpper(current) ? 'Ь' : 'ь');
                }
                else
                    sb.Append(current);
            }

            var endsWithParenthesis = text.EndsWith(')') || text.EndsWith('(');
            sb.Append((RandomNumberGenerator.GetInt32(100) % 3) switch
            {
                1 when !endsWithParenthesis => "))0)",
                2 when !endsWithParenthesis => "((9(",
                _ => string.Empty
            });

            await client.SendMessageAsync(message.Chat.Id, sb.ToString(),
                replyParameters: message.CreateReplyParameters(), cancellationToken: cancellationToken);
        }
    }

    protected override Task OnCommandAsync(Message message, string commandName, string args,
        CancellationToken cancellationToken = default)
    {
        var command = commands.FirstOrDefault(c => c.Command == commandName);
        return command is null
            ? Task.CompletedTask
            : command.ExecuteAsync(client, repository, _botInfo, message, cancellationToken);
    }

    protected override async Task OnEditedMessageAsync(Message message, CancellationToken cancellationToken = default)
    {
        if (await repository.GetEntryOrDefaultAsync(message.From?.Id, message.Chat.Id, cancellationToken) is not null)
            return;

        await client.SetMessageReactionAsync(message.Chat.Id, message.MessageId,
            await spellCheckerService.IsTextSpelledCorrectlyAsync(message.Text ?? message.Caption, cancellationToken)
                ? null
                : Helpers.DefaultReactions,
            false, cancellationToken);
    }
}