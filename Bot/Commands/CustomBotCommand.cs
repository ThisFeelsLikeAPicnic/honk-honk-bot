﻿using System.Collections.Immutable;
using HonkHonkBot.Repository;
using Telegram.BotAPI;
using Telegram.BotAPI.AvailableMethods;
using Telegram.BotAPI.AvailableTypes;

namespace HonkHonkBot.Bot.Commands;

public abstract class CustomBotCommand(string commandName, string description, ILogger<CustomBotCommand> logger)
    : BotCommand(commandName, description)
{
    private readonly ImmutableArray<ReactionTypeEmoji> _successReactions =
        ImmutableArray.Create(new ReactionTypeEmoji { Emoji = "👍" });

    private readonly ImmutableArray<ReactionTypeEmoji> _failedReactions =
        ImmutableArray.Create(new ReactionTypeEmoji { Emoji = "👎" });

    public async Task ExecuteAsync(ITelegramBotClient client, IRepository repository, User botInfo, Message message,
        CancellationToken cancellationToken)
    {
        logger.LogInformation("Received command - name:{Command}, message_id:{MessageId}, chat_id:{ChatId}",
            Command, message.MessageId, message.Chat.Id);

        var chatOwner = default(ChatMember);
        try
        {
            chatOwner = (await client.GetChatAdministratorsAsync(message.Chat.Id, cancellationToken))
                .FirstOrDefault(m => m is ChatMemberOwner);
        }
        catch (BotRequestException)
        {
            // we're likely in a private chat, ignore
        }

        var chatSettings = await repository.GetChatSettingsOrDefaultAsync(message.Chat.Id, cancellationToken);
        if (chatSettings is null)
        {
            await repository.CreateChatSettingsAsync(message.Chat.Id, chatOwner?.User.LanguageCode ?? "en",
                cancellationToken);

            chatSettings = await repository.GetChatSettingsOrDefaultAsync(message.Chat.Id, cancellationToken);
        }
        else if (chatSettings.Language.IsNullOrEmptyOrWhiteSpace())
        {
            chatSettings.Language = chatOwner?.User.LanguageCode?.IsNullOrEmptyOrWhiteSpace() is not false
                ? "en"
                : chatOwner.User.LanguageCode;

            await repository.UpdateChatSettingsAsync(chatSettings, cancellationToken);
        }

        var commandArgs = new CommandArgs(botInfo, message, chatOwner, chatSettings!);
        var result = await ExecuteInnerAsync(repository, commandArgs, cancellationToken);
        if (result is null)
            return;

        var isQuietModeEnabled = result.NewChatSettings?.QuietMode ?? false;
        if (isQuietModeEnabled || result.Message.IsNullOrEmptyOrWhiteSpace())
            await client.SetMessageReactionAsync(message.Chat.Id, message.MessageId, result.Type switch
            {
                CommandResultTypes.Success => _successReactions,
                CommandResultTypes.Fail => _failedReactions,
                _ => Helpers.DefaultReactions
            }, isQuietModeEnabled, cancellationToken);
        else
            await client.SendMessageAsync(message.Chat.Id, result.Message!,
                replyParameters: chatOwner is null
                    ? null
                    : new ReplyParameters
                    {
                        ChatId = message.Chat.Id,
                        MessageId = message.MessageId,
                        AllowSendingWithoutReply = true
                    },
                cancellationToken: cancellationToken);
    }

    protected abstract Task<CommandResult?> ExecuteInnerAsync(IRepository repository, CommandArgs commandArgs,
        CancellationToken cancellationToken = default);
}

#region Type declarations

public record CommandArgs(User BotInfo, Message Message, ChatMember? ChatOwner, ChatSettings ChatSettings);

public record CommandResult(CommandResultTypes Type, string? Message = null, ChatSettings? NewChatSettings = null);

public enum CommandResultTypes
{
    Success,
    Fail,
    Forbidden,
    NotModified
}

#endregion