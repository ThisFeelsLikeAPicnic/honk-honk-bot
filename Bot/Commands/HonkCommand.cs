﻿using HonkHonkBot.Repository;

namespace HonkHonkBot.Bot.Commands;

public class HonkCommand(ILogger<HonkCommand> logger)
    : CustomBotCommand("honk", "Bestow the HONK-HONK curse via reply", logger)
{
    protected override async Task<CommandResult?> ExecuteInnerAsync(IRepository repository, CommandArgs commandArgs,
        CancellationToken cancellationToken = default)
    {
        var targetUser = commandArgs.Message.ReplyToMessage is { } originalMessage
            ? originalMessage.From
            : commandArgs.Message.From;

        if (targetUser?.Id == commandArgs.BotInfo.Id)
            return new CommandResult(CommandResultTypes.Forbidden, "...dude");

        var forSelf = targetUser?.Id == commandArgs.Message.From?.Id;
        var entry = await repository.GetEntryOrDefaultAsync(targetUser?.Id, commandArgs.Message.Chat.Id,
            cancellationToken);

        if (entry is not null)
            return new CommandResult(CommandResultTypes.NotModified,
                $"{(forSelf ? "You" : "They")}'re already cursed, stupid motherfucker.");

        if (targetUser?.Id == commandArgs.ChatOwner?.User.Id && !forSelf)
            return new CommandResult(CommandResultTypes.Forbidden,
                "You can't honk-honk the gods, stupid motherfucker.");

        return await repository.CreateEntryAsync(targetUser?.Id, commandArgs.Message.Chat.Id, cancellationToken) > 0
            ? new CommandResult(CommandResultTypes.Success, forSelf
                ? "YOU'VE CURSED YOURSELF..."
                : "I BESTOW THE HONK-HONK CURSE UPON THEE!")
            : new CommandResult(CommandResultTypes.Fail);
    }
}