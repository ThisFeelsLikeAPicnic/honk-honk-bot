﻿using HonkHonkBot.Repository;

namespace HonkHonkBot.Bot.Commands;

public class ShushCommand(ILogger<ShushCommand> logger) : CustomBotCommand("shush", "i be quiet okay...", logger)
{
    protected override async Task<CommandResult?> ExecuteInnerAsync(IRepository repository, CommandArgs commandArgs,
        CancellationToken cancellationToken = default)
    {
        if (commandArgs.ChatSettings.QuietMode)
            return new CommandResult(CommandResultTypes.NotModified);

        if (commandArgs.ChatOwner?.User.Id != commandArgs.Message.From?.Id)
            return new CommandResult(CommandResultTypes.Forbidden, "Mortals can't silence me, stupid motherfucker.");

        commandArgs.ChatSettings.QuietMode = true;
        return await repository.UpdateChatSettingsAsync(commandArgs.ChatSettings, cancellationToken) > 0
            ? new CommandResult(CommandResultTypes.Success, NewChatSettings: commandArgs.ChatSettings)
            : new CommandResult(CommandResultTypes.Fail, "I'M FUCKING INVINCIBLE!");
    }
}