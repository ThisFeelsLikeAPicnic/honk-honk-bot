﻿using HonkHonkBot.Repository;

namespace HonkHonkBot.Bot.Commands;

public class UnhonkCommand(ILogger<UnhonkCommand> logger)
    : CustomBotCommand("unhonk", "Remove the HONK-HONK curse via reply", logger)
{
    protected override async Task<CommandResult?> ExecuteInnerAsync(IRepository repository, CommandArgs commandArgs,
        CancellationToken cancellationToken = default)
    {
        var targetUser = commandArgs.Message.ReplyToMessage is { } originalMessage
            ? originalMessage.From
            : commandArgs.Message.From;

        if (targetUser?.Id == commandArgs.BotInfo.Id)
            return new CommandResult(CommandResultTypes.Forbidden);

        var entry = await repository.GetEntryOrDefaultAsync(targetUser?.Id, commandArgs.Message.Chat.Id,
            cancellationToken);

        if (entry is null)
            return new CommandResult(CommandResultTypes.NotModified,
                $"{(targetUser?.Id == commandArgs.Message.From?.Id ? "You" : "They")} aren't cursed, stupid motherfucker.");

        if (commandArgs.Message.From?.Id != commandArgs.ChatOwner?.User.Id)
            return new CommandResult(CommandResultTypes.Forbidden,
                "Only gods can help the cursed ones... stupid motherfucker.");

        return await repository.RemoveEntryAsync(entry, cancellationToken) > 0
            ? new CommandResult(CommandResultTypes.Success, "THE GODS HAVE LIFTED THE CURSE FROM THEE!")
            : new CommandResult(CommandResultTypes.Fail);
    }
}