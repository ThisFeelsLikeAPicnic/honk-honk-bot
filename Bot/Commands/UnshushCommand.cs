﻿using HonkHonkBot.Repository;

namespace HonkHonkBot.Bot.Commands;

public class UnshushCommand(ILogger<UnshushCommand> logger)
    : CustomBotCommand("unshush", "OH SHIT HERE WE GO AGAIN", logger)
{
    protected override async Task<CommandResult?> ExecuteInnerAsync(IRepository repository, CommandArgs commandArgs,
        CancellationToken cancellationToken = default)
    {
        if (!commandArgs.ChatSettings.QuietMode)
            return new CommandResult(CommandResultTypes.NotModified, "Are you stupid or what?");

        if (commandArgs.ChatOwner?.User.Id != commandArgs.Message.From?.Id)
            return new CommandResult(CommandResultTypes.Forbidden);

        commandArgs.ChatSettings.QuietMode = false;
        return await repository.UpdateChatSettingsAsync(commandArgs.ChatSettings, cancellationToken) > 0
            ? new CommandResult(CommandResultTypes.Success, "EY b0SS GIBE DE HONK b0SS",
                NewChatSettings: commandArgs.ChatSettings)
            : new CommandResult(CommandResultTypes.Fail);
    }
}