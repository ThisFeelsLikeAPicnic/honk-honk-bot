﻿using HonkHonkBot.Bot.Jokes;
using HonkHonkBot.Repository;

namespace HonkHonkBot.Bot.Commands;

public class WisdomBotCommand(IServiceProvider serviceProvider, ILogger<CustomBotCommand> logger)
    : CustomBotCommand("wisdom", "Dispense wisdom from my mighty wisdom tooth", logger)
{
    protected override async Task<CommandResult?> ExecuteInnerAsync(IRepository repository, CommandArgs commandArgs,
        CancellationToken cancellationToken = default)
    {
        if (commandArgs.ChatSettings.QuietMode)
            return new CommandResult(CommandResultTypes.NotModified);

        var joke = await serviceProvider
            .GetRequiredKeyedService<IJokeService>(LanguageCodes.Russian)
            .GetRandomJokeAsync(cancellationToken);

        return joke.IsNullOrEmptyOrWhiteSpace()
            ? new CommandResult(CommandResultTypes.Fail, "You are not worthy of my wisdom, stupid motherfucker.")
            : new CommandResult(CommandResultTypes.Success, joke);
    }
}