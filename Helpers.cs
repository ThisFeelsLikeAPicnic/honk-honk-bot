﻿using System.Collections.Immutable;
using System.Security.Cryptography;
using Telegram.BotAPI.AvailableTypes;

namespace HonkHonkBot;

public static class Helpers
{
    public static ImmutableArray<ReactionTypeEmoji> DefaultReactions { get; } =
        ImmutableArray.Create(new ReactionTypeEmoji { Emoji = "🤡" });

    public static bool IsNullOrEmptyOrWhiteSpace(this string? text) =>
        text is null || text.Length == 0 || text.All(char.IsWhiteSpace);

    public static DateTime GetRandomNextTimestamp(this DateTime timestamp)
    {
        var newTimestamp = new DateTime(timestamp.Year, timestamp.Month, timestamp.Day);

        // 1d = 24h = 1440m = 86400s
        var randomTimeOfDayInSeconds = RandomNumberGenerator.GetInt32(1, 86400);
        return newTimestamp.AddDays(1).AddSeconds(randomTimeOfDayInSeconds);
    }

    public static ReplyParameters CreateReplyParameters(this Message message) => new()
    {
        ChatId = message.Chat.Id,
        MessageId = message.MessageId,
        AllowSendingWithoutReply = true
    };

    public static async Task ProcessArrayInChunksWithDelayAsync<T>(this IEnumerable<T> items, int chunkSize, TimeSpan delay,
        Func<T, Task> itemAction, CancellationToken cancellationToken)
    {
        foreach (var itemsChunk in items.Chunk(chunkSize))
        {
            var startTime = DateTime.UtcNow;
            foreach (var item in itemsChunk)
                await itemAction(item);

            var processingTime = DateTime.UtcNow - startTime;
            if (processingTime < delay)
                await Task.Delay(delay - processingTime, cancellationToken);
        }
    }

    public static T GetRandomItem<T>(this IList<T> items) =>
        items[RandomNumberGenerator.GetInt32(items.Count)];
}