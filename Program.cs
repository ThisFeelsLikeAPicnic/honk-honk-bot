﻿// DISCLAIMER: you'll notice a couple of slurs here and there
// these slurs are nothing more than author's artistic choice
// you can remove them, if you like

using System.Text;
using Catalyst;
using HonkHonkBot;
using HonkHonkBot.Bot;
using HonkHonkBot.Bot.Commands;
using HonkHonkBot.Bot.Jokes;
using HonkHonkBot.Repository;
using Mosaik.Core;
using SQLite;
using Telegram.BotAPI;

Storage.Current = new DiskStorage(Environment.GetEnvironmentVariable("CATALYST_MODELS_DIR") ?? "catalyst-models");
Catalyst.Models.Russian.Register();
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
var builder = Host.CreateApplicationBuilder(args);

builder.Logging.ClearProviders();
builder.Logging.AddSimpleConsole(o => o.SingleLine = true);

builder.Services.Configure<BotSettings>(builder.Configuration.GetSection(nameof(BotSettings)));
builder.Services.AddSingleton<SQLiteAsyncConnection>(_ =>
{
    var dbPath = Environment.GetEnvironmentVariable("DB_PATH");
    if (dbPath.IsNullOrEmptyOrWhiteSpace())
        dbPath = "./database/storage.db";

    var dbDirectory = Directory.GetParent(dbPath!);
    if (dbDirectory?.Exists is false)
        dbDirectory.Create();

    return new SQLiteAsyncConnection(dbPath);
});

builder.Services.AddSingleton<IRepository, SqliteRepository>();
builder.Services.AddSingleton<ITelegramBotClient, TelegramBotClient>(_ =>
{
    var botToken = Environment.GetEnvironmentVariable("BOT_TOKEN");
    ArgumentException.ThrowIfNullOrEmpty(botToken, "BOT_TOKEN");
    ArgumentException.ThrowIfNullOrWhiteSpace(botToken, "BOT_TOKEN");

    return new TelegramBotClient(botToken);
});

builder.Services.AddHttpClient<IJokeService, RuV2JokeService>();
builder.Services.AddHttpClient<IJokeService, EnJokeService>();
builder.Services.AddHttpClient<ISpellCheckerService, SpellCheckerService>();
builder.Services.AddKeyedSingleton<IJokeService, RuV2JokeService>(LanguageCodes.Russian);
builder.Services.AddKeyedSingleton<IJokeService, EnJokeService>(LanguageCodes.English);
builder.Services.AddSingleton<ISpellCheckerService, SpellCheckerService>();
builder.Services.AddSingleton<CustomBotCommand, HonkCommand>();
builder.Services.AddSingleton<CustomBotCommand, UnhonkCommand>();
builder.Services.AddSingleton<CustomBotCommand, ShushCommand>();
builder.Services.AddSingleton<CustomBotCommand, UnshushCommand>();
builder.Services.AddSingleton<CustomBotCommand, WisdomBotCommand>();
builder.Services.AddSingleton<Pipeline>(_ => Pipeline.For(Language.Russian));
builder.Services.AddSingleton<ICustomTelegramBot, CustomTelegramBot>();
builder.Services.AddHostedService<JokesBackgroundService>();
builder.Services.AddHostedService<BotBackgroundService>();

var host = builder.Build();
await host.RunAsync();